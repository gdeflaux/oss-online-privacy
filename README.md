# An Attempt To Open Source & Online Privacy

Objectives:

- Use Open Source software
- Protect my privacy

## Laptop

### Hardware

**Lenovo X1 Carbon 6th Gen**

Why this Laptop?

- This model has overall great Linux support and all distributions I've tried worked perfectly (Ubuntu, POP_OS, Linux Mint, ElementaryOS).
- This laptop is very light.

Specifications:

- Processor - Intel i7-8650U (8) @ 4.200GHz
    - I wanted something fast.
- RAM - 16GB
    - I wanted something fast. 
- Display - FHD (1900x1200)
    - Linux doesn't have great support for fractional scaling so 4K displays don't look great. 4K displays are also tend to drain the battery.
    - On this display option Lenovo offers a built-in camera shutter.
    - No touchscreen since popular Linux window managers are not touch ready.

### Software

**Operating System:** Elementary OS

- I was coming from Mac so I was looking as something with a similar user experience and consistency in UI.
- I was looking at a distribution that came with good defaults requiring minimal tweaking.
- AppCenter: I like to pay-as-you-want approach and the fact that apps were curated.

**Browser:** Firefox
- Extensions:
    - Privacy Badger
    - uBlock Orrigin
    - HTTPS Everywhere
    - Decentraleyes
- Search Engine: DuckDuckGo

**Note Taking:** Joplin

**Office :** Libre Office

**Torrent Download**: Torrential

**Backup:** Timeshift

**Image Manipulation:** GIMP

**Vector Drawing:** Inkscape

**Gaming**: Steam

**Other Tools:**
- Autokey
- Caffeine 


## Mobile

Protecting your privacy in the realm of mobile operating systems basically means to reduce as much as possible the data that is send to Google ; Android being the only open source operating system with a real ecosystem of applications. AOSP still sends a lot of data to Google.

By choosing alternative Android operating systems you loose in functionality and some applications won't work.

### Hardware

OnePlus 5T

- Supported by LineageOS
- Good performance for an _affordable_ price
- Has a fingerprint scanner
- Reasonable form factor and size (not too big)

### Software

**Operating System:** MicroG for LineageOS
- This version of LineageOS comes with MicroG and no Google Apps are included.
- MicroG is a re-implementation of the Google Services API which limits the amount of data sent to Google by using privacy-respecting services. Eg: Mozilla is used for location services.
- Weekly updates are available with upstream LineageOS.

**App Store:**
- Open Source apps: F-Droid
- PlayStore apps: Aurora

**Contacts & Calendar:** DAVx
- Seamlessly syncs contacts and calendards from Nextcloud.

## Hosted Services

**Email:** ProtonMail

**VPN:** ProtonVPN

**Cloud:** Nextcloud at Hetzner
- Applications:
    - Files
    - Contacts
    - Calendar

**Password Manager:** Bitwarden
